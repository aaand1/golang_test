#!/usr/bin/env sh

sudo service docker start
sudo service mongodb start

echo "running integration tests(with unit-tests on multistage build)"
docker-compose --project-directory ./ -f ./build/package/clientapi/Docker-compose.test-integration.yaml up --build --abort-on-container-exit
echo "Cleaning..."
docker stop portdomain-service
docker stop portdomain-mongo
docker stop clientapi-service


echo "creating network golang_test..."
docker network create -d bridge golang_test
echo "building containerized services..."
docker build --force-rm --target stage -t golang_test/portdomain-service:latest -f ./build/package/portdomain/Dockerfile .
docker build --force-rm --target stage -t golang_test/clientapi-service:latest -f ./build/package/clientapi/Dockerfile .
echo "done"

echo "starting portdomain-db..."
docker run --rm -d --name="portdomain-mongo" -p 27017:27017 --network golang_test mongo:4.2.3-bionic
mongodb_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' portdomain-mongo)
echo "done"

echo "starting portdomain-service..."
# TODO: fix port exposure
docker run --rm -d --name=portdomain-service -p 8081:8081 \
    -e PORT_DOMAIN_SERVICEHOST=portdomain-service -e PORT_DOMAIN_SERVICEPORT=8081 \
    -e PORT_DOMAIN_LOGLEVEL=trace -e PORT_DOMAIN_CONNECTIONSTRING="mongodb://portdomain-mongo:27017" --network golang_test golang_test/portdomain-service:latest
echo "done"

echo "starting clientapi-service..."
docker run --rm -d --name=clientapi-service -p 8080:8080 \
    -e CLIENT_API_SERVICEHOST=clientapi-service -e CLIENT_API_SERVICEPORT=8080 \
    -e CLIENT_API_PORTDOMAINSERVICEADDRESS="http://portdomain-service:8081/" --network golang_test golang_test/clientapi-service:latest
clientapi_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' clientapi-service)
echo "done"

sleep 2s
echo "Uploading json..."
ports_json=$(cat ./test/testdata/ports.json)
post_result=$(curl -XPOST -H "Accept application/json" -H "Content-Type: application/json" -d @./test/testdata/ports.json -s http://$clientapi_ip:8080/api/ports )
echo "Done. res=$post_result"
echo "Uploaded counters" $(echo "$post_result" | jq ".stats")

sleep 5s
echo "Fetching uploaded json(1400-1600):"
fetch_result=$(curl -XGET -H "Accept application/json" -s http://$clientapi_ip:8080/api/ports?offset=1400&limit=200 )
echo "\ttotal fetched: " $(echo "$fetch_result" | jq ".items | length")
echo "\ttotal items: " $(echo "$fetch_result" | jq ".paging.total")
total_uploaded=$(echo "$post_result" | jq ".stats.totalInserted")
fetched_total=$(echo "$fetch_result" | jq ".paging.total")

echo "cheching counters..."
if [ "$total_uploaded" != "$fetched_total" ]; then
    echo "ERROR: $total_uploaded != $fetched_total"
else
    echo "OK"
fi

echo "cleaning up..."

for C in "portdomain-service clientapi-service portdomain-mongo";
do
  docker container stop $C
done
#docker network rm golang_test
