package controller

import (
	"bitbucket.org/aaand1/golang_test/services/clientapi/clients"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
)

func InitControllers(conn grpc.ClientConnInterface) *portsController {
	ports := &portsController{clients.NewPortDomainServiceClient(conn)}

	return ports
}

func Startup(conn grpc.ClientConnInterface, appRouter *gin.Engine, apiRouter *gin.Engine) *portsController {
	ports := InitControllers(conn)

	ports.registerRoutes(appRouter, apiRouter)
	return ports
}
