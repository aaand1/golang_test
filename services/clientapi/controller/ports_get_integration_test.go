// +build integration

package controller

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"sort"
	"strings"
	"testing"
)

type PortsGetSuite struct {
	suite.Suite

	*portsUtils
}

func NewPortsGetSuite(sourcePath string) *PortsGetSuite {
	return &PortsGetSuite{portsUtils: NewPortsUtils(sourcePath)}
}

func (s *PortsGetSuite) SetupSuite() {
	resp, err := s.portsUtils.uploadPorts(s.portsUtils.ports)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
}

func (s *PortsGetSuite) TearDownSuite() {
	//s.portsUtils.deletePorts(s.portsUtils.ports)
}

func (s *PortsGetSuite) Test_ports_Get() {
	type args struct {
		skip, limit string
	}

	type expectations struct {
		expectedStatusCode int
		expectedResponse   struct {
			paging    map[string]int
			items     []map[string]interface{}
			errorCode string
		}
	}

	portsResponses := s.getPortsResponses()
	getPorts := func(skip, limit int) []map[string]interface{} {
		return portsResponses[skip : skip+limit]
	}

	tests := []struct {
		name         string
		args         *args
		expectations expectations
	}{
		{
			"It should return 200/OK for fetch of ports",
			nil, expectations{
				http.StatusOK, struct {
					paging    map[string]int
					items     []map[string]interface{}
					errorCode string
				}{paging: map[string]int{"skip": 0, "limit": 30, "total": len(s.portsUtils.ports)}, items: getPorts(0, 30)},
			},
		}, {
			"It should return 200/OK for fetch of ports with skip=10&limit=50",
			&args{
				skip:  "10",
				limit: "50",
			}, expectations{
				http.StatusOK, struct {
					paging    map[string]int
					items     []map[string]interface{}
					errorCode string
				}{paging: map[string]int{"skip": 10, "limit": 50, "total": len(s.portsUtils.ports)}, items: getPorts(10, 50)},
			},
		}, {
			"It should return 400/BAD_REQUEST/{code:'INVALID_QUERY_PARAMS'} for fetch of ports with skip=abc&limit=10",
			&args{
				skip:  "abc",
				limit: "10",
			}, expectations{
				http.StatusBadRequest, struct {
					paging    map[string]int
					items     []map[string]interface{}
					errorCode string
				}{errorCode: "INVALID_QUERY_PARAMS"},
			},
		}, {
			"It should return 400/BAD_REQUEST/{code:'INVALID_QUERY_PARAMS'} for fetch of ports with limit=qwe",
			&args{
				skip:  "0",
				limit: "qwe",
			}, expectations{
				http.StatusBadRequest, struct {
					paging    map[string]int
					items     []map[string]interface{}
					errorCode string
				}{errorCode: "INVALID_QUERY_PARAMS"},
			},
		},
	}

	for _, it := range tests {
		s.T().Run(it.name, func(t *testing.T) {
			//Arrange
			reqUrl, _ := url.Parse(runConfig.ClientApiServiceBaseUrl + "api/ports")
			if it.args != nil {
				query := reqUrl.Query()
				query.Set("skip", it.args.skip)
				query.Set("limit", it.args.limit)
				reqUrl.RawQuery = query.Encode()
			}

			//Act
			resp, err := http.Get(reqUrl.String())
			require.NoError(t, err)
			defer resp.Body.Close()

			//Assert
			require.Equal(t, it.expectations.expectedStatusCode, resp.StatusCode,
				"expected status code to equal %v, but got %v", it.expectations.expectedStatusCode, resp.StatusCode)

			var data map[string]interface{}
			err = json.NewDecoder(resp.Body).Decode(&data)
			require.NoError(t, err)

			expectedResp := it.expectations.expectedResponse
			if expectedResp.errorCode != "" {
				require.Equal(t, data["code"], expectedResp.errorCode)
				return
			}

			require.JSONEq(t, mustStringify(data["paging"], true), mustStringify(expectedResp.paging, true))

			actualResp := data["items"].([]interface{})
			for i, it := range expectedResp.items {
				require.EqualValues(t, actualResp[i], it)
			}
		})
	}
}

func (s *PortsGetSuite) getPortsResponses() []map[string]interface{} {
	portsResponses := []map[string]interface{}{}
	for portLocode, port := range s.portsUtils.ports {
		responseItem := map[string]interface{}{
			"locode": portLocode,
		}
		for optionalField, defaultVal := range map[string]interface{}{
			"coordinates": []float64{},
			"province":    "",
			"timezone":    "",
			"code":        "",
		} {
			if val, ok := port[optionalField]; !ok {
				responseItem[optionalField] = defaultVal
			} else {
				responseItem[optionalField] = val
			}
		}
		json.NewDecoder(strings.NewReader(mustStringify(port, true))).Decode(&responseItem)
		portsResponses = append(portsResponses, responseItem)
	}
	sort.SliceStable(portsResponses, func(i, j int) bool {
		return strings.Compare(portsResponses[i]["locode"].(string), portsResponses[j]["locode"].(string)) == -1
	})
	return portsResponses
}

func Test_ports_Get(t *testing.T) {
	cwd, _ := os.Getwd()
	suite.Run(t, NewPortsGetSuite(path.Join(cwd, "../../../test/testdata/ports.json")))
}
