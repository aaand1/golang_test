// +build integration

package controller

import (
	"bytes"
	"encoding/json"
	"github.com/kelseyhightower/envconfig"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"sync"
	"testing"
)

type integrationTestsConfig struct {
	ClientApiServiceBaseUrl string `envconfig:"CLIENT_API_SERVICE_BASE_URL" default:"http://localhost:8080/"`
	LogLevel                string `default:"debug"`
}

var (
	runConfig = &integrationTestsConfig{}
)

func TestMain(m *testing.M) {
	envconfig.MustProcess("CLIENT_API_INTEGRATION_TESTS", runConfig)
	//log.Init(runConfig.LogLevel, os.Stdout, "", &logrus.TextFormatter{FullTimestamp: true, TimestampFormat: time.StampMicro})
	result := m.Run()
	os.Exit(result)
}

func mustStringify(obj interface{}, encode bool) string {
	var err error
	var res []byte
	if encode {
		buf := bytes.NewBuffer(res)
		err = json.NewEncoder(buf).Encode(obj)
		res = buf.Bytes()
	} else {
		res, err = json.Marshal(obj)
	}
	if err != nil {
		panic(err)
	}
	return string(res)
}

type portsReqData map[string]portItem
type portItem map[string]interface{}

type portsUtils struct {
	ports portsReqData
}

func NewPortsUtils(sourcePath string) *portsUtils {
	dataPath, _ := filepath.Abs(sourcePath)
	portsData, err := ioutil.ReadFile(dataPath)
	if err != nil {
		log.Fatal(err)
	}

	var ports portsReqData
	if err = json.Unmarshal(portsData, &ports); err != nil {
		log.Fatal(err)
	}

	return &portsUtils{ports}
}

func (utils *portsUtils) prepareMultipartRequest(url string, formParamName string, portsData portsReqData) (req *http.Request, err error) {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(formParamName, "ports.json")
	if err != nil {
		return
	}
	fileContents, err := json.Marshal(portsData)
	if err != nil {
		return
	}
	part.Write(fileContents)
	defer writer.Close()

	req, err = http.NewRequest(http.MethodPost, url, body)
	if err != nil {
		return
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())
	return
}

func (utils *portsUtils) uploadPorts(ports portsReqData) (*http.Response, error) {
	if ports == nil {
		ports = utils.ports
	}
	//req, err := utils.prepareMultipartRequest(runConfig.ClientApiServiceBaseUrl+"api/ports", "file", ports)
	req, err := http.NewRequest(http.MethodPost, runConfig.ClientApiServiceBaseUrl+"api/ports",
		strings.NewReader(mustStringify(ports, false)))
	if err != nil {
		return nil, err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	return resp, err
}

func (utils *portsUtils) deletePorts(ports portsReqData) error {
	wg := &sync.WaitGroup{}

	for portLocode, portData := range ports {
		wg.Add(1)

		go func(portLocode string, portData portItem) {
			defer wg.Done()
			request, _ := http.NewRequest(http.MethodDelete,
				runConfig.ClientApiServiceBaseUrl+"api/ports/"+portLocode, nil)
			resp, _ := http.DefaultClient.Do(request)

			//if err != nil {
			//log.Logger.Errorf("FAILED TO DELETE port with LOCODE=%v. err=%s\n", portLocode, err)
			//require.NoError(s.T(), err)
			//}
			defer resp.Body.Close()
		}(portLocode, portData)
	}

	wg.Wait()
	return nil
}

type PortsPostSuite struct {
	suite.Suite

	*portsUtils
}

func NewPortsPostSuite(sourcePath string) *PortsPostSuite {
	return &PortsPostSuite{portsUtils: NewPortsUtils(sourcePath)}
}

func (s *PortsPostSuite) SetupSuite() {
	//s.portsUtils.uploadPorts(s.portsUtils.ports)
}

func (s *PortsPostSuite) TearDownSuite() {
	//s.portsUtils.deletePorts(s.portsUtils.ports)
}

func (s *PortsPostSuite) Test_ports_Post() {
	type args struct {
		portsData portsReqData
	}

	type expectations struct {
		expectedStatusCode int
		expectedResponse   map[string]interface{}
	}
	existingKeys := [...]string{"UAIEV", "UAODS", "UAKHE"}

	getPorts := func(res map[string]portItem, keys ...string) map[string]portItem {
		for _, key := range keys {
			res[key] = s.portsUtils.ports[key]
		}
		return (res)
	}

	tests := []struct {
		name         string
		args         args
		expectations expectations
	}{
		{
			"It should return 201/CREATED for post of NEW ports",
			args{
				s.portsUtils.ports,
			}, expectations{
				http.StatusCreated, map[string]interface{}{
					"stats": map[string]interface{}{
						"totalInserted": len(s.portsUtils.ports),
						"totalUpdated":  0,
					},
				}},
		}, {
			"It should return 200/OK for update of EXISTING ports",
			args{
				getPorts(map[string]portItem{
					"UAODS.UASVP": map[string]interface{}{
						"name":        "Odessa.Sevastopol",
						"city":        "Odessa",
						"country":     "Ukraine",
						"alias":       []string{},
						"regions":     []string{},
						"coordinates": [...]float32{30.7233095, 46.482526},
						"province":    "Odessa Oblast",
						"timezone":    "Europe/Kiev",
						"unlocs":      []string{"UAODS"},
						"code":        "46275",
					},
				}, existingKeys[:]...),
			}, expectations{
				http.StatusOK, map[string]interface{}{
					"stats": map[string]interface{}{
						"totalInserted": 1,
						"totalUpdated":  len(existingKeys),
					},
				}},
		},
	}

	for _, it := range tests {
		s.T().Run(it.name, func(t *testing.T) {
			//Arrange
			reqUrl, err := url.Parse(runConfig.ClientApiServiceBaseUrl + "api/ports")
			if err != nil {
				log.Fatal(err)
			}

			//Act
			resp, err := http.Post(reqUrl.String(), "application/json", strings.NewReader(mustStringify(it.args.portsData, false)))
			//resp, err := s.portsUtils.uploadPorts(it.args.portsData)
			require.NoError(t, err)
			defer resp.Body.Close()

			//Assert
			assert.Equal(t, it.expectations.expectedStatusCode, resp.StatusCode,
				"expected status code to equal %v, but got %v", it.expectations.expectedStatusCode, resp.StatusCode)

			data, err := ioutil.ReadAll(resp.Body)
			require.NoError(t, err)
			require.Equal(t, mustStringify(it.expectations.expectedResponse, true), string(data), "Expected response to be '%+v', but got %+v",
				it.expectations.expectedResponse, string(data))
		})
	}
}

func Test_ports_Batch_Post(t *testing.T) {
	cwd, _ := os.Getwd()
	suite.Run(t, NewPortsPostSuite(path.Join(cwd, "../../../test/testdata/ports.json")))
}
