package controller

import (
	"bitbucket.org/aaand1/golang_test/pkg/log"
	"bitbucket.org/aaand1/golang_test/services/clientapi/clients"
	"bitbucket.org/aaand1/golang_test/services/clientapi/viewmodels"
	"github.com/gin-gonic/gin"
	"github.com/mailru/easyjson"
	"net/http"
)

type portsController struct {
	portDomainClient clients.IPortDomainServiceClient
}

func (pc *portsController) registerRoutes(appRouter *gin.Engine, apiRouter *gin.Engine) {
	apiRouter.POST("/api/ports", pc.handleUpsertPorts)
	apiRouter.GET("/api/ports", pc.handleGetAll)
}

func (pc *portsController) handleUpsertPorts(c *gin.Context) {
	var portsViewModels = viewmodels.PortsItems{}
	err := easyjson.UnmarshalFromReader(c.Request.Body, &portsViewModels)
	defer c.Request.Body.Close()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, map[string]interface{}{
			"error": err.Error(),
			"code":  "INVALID_DATA",
		})
		return
	}

	var portsModels = portsViewModels.MapToModels()
	totalInserted, totalUpdated, err := pc.portDomainClient.UpsertPorts(portsModels)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, map[string]interface{}{
			"error": err.Error(),
			"code":  "INTERNAL_SERVER_ERROR",
		})
		return
	}

	status := http.StatusCreated
	if totalUpdated > 0 || totalInserted < len(portsModels) {
		status = http.StatusOK
	}
	c.JSON(status, gin.H{
		"stats": gin.H{
			"totalInserted": totalInserted,
			"totalUpdated":  totalUpdated,
		},
	})
}

func (pc *portsController) handleGetAll(c *gin.Context) {
	var viewModel viewmodels.GetAllViewModel
	if err := c.ShouldBindQuery(&viewModel); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"code": "INVALID_QUERY_PARAMS", "error": ""})
		return
	}
	data, paging, err := pc.portDomainClient.GetPortsList(viewModel.Limit, viewModel.Skip)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"code": "INTERNAL_SERVER_ERROR", "error": err.Error()})
		return
	}

	if len(data) > 0 {
		log.Logger.Debugf("response: paging=%+v items(%v)=%+v-%+v\n", paging, len(data), data[0].Locode, data[len(data)-1].Locode)
	}
	c.JSON(http.StatusOK, gin.H{
		"paging": gin.H{
			"total": paging["total"],
			"skip":  paging["skip"],
			"limit": paging["limit"],
		},
		"items": data,
	})
}
