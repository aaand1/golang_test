package viewmodels

type GetAllViewModel struct {
	Skip  int32 `form:"skip,default=0" binding:"min=0"`
	Limit int32 `form:"limit,default=30" binding:"min=0,max=200"`
}
