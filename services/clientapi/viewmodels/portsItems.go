package viewmodels

import (
	"bitbucket.org/aaand1/golang_test/pkg/models"
)

type PortItem struct {
	Name        string     `json:"name"`
	City        string     `json:"city"`
	Country     string     `json:"country"`
	Alias       []string   `json:"alias"`
	Regions     []string   `json:"regions"`
	Coordinates [2]float64 `json:"coordinates"` //Optional(n=24), 1608/1632
	Province    string     `json:"province"`    //Optional(n=67), 1565/1632
	Timezone    string     `json:"timezone"`    //Optional(n=44), 1588/1632
	Unlocs      []string   `json:"unlocs"`
	Code        string     `json:"code"` //Optional(n=337), 1295/1632
}

//easyjson:json
type PortsItems map[string]PortItem

func (pItems *PortsItems) MapToModels() []*models.PortItem {
	portsModels := make([]*models.PortItem, len(*pItems))
	i := 0
	for portLocode, p := range *pItems {
		portsModels[i] = &models.PortItem{
			Locode:      portLocode,
			Name:        p.Name,
			City:        p.City,
			Country:     p.Country,
			Alias:       p.Alias,
			Regions:     p.Regions,
			Coordinates: p.Coordinates,
			Province:    p.Province,
			Timezone:    p.Timezone,
			Unlocs:      p.Unlocs,
			Code:        p.Code,
		}
		i++
	}

	return portsModels
}
