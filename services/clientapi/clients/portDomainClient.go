package clients

import (
	"bitbucket.org/aaand1/golang_test/internal/gRPC"
	"bitbucket.org/aaand1/golang_test/pkg/models"
	"context"
	"google.golang.org/grpc"
)

type IPortDomainServiceClient interface {
	UpsertPorts([]*models.PortItem) (int, int, error)
	GetPortsList(int32, int32) ([]*models.PortItem, map[string]int, error)
}

type portDomainServiceClientImpl struct {
	rpcClient gRPC.PortDomainServiceClient
	ctx       context.Context
}

func NewPortDomainServiceClient(cc grpc.ClientConnInterface) IPortDomainServiceClient {
	return &portDomainServiceClientImpl{gRPC.NewPortDomainServiceClient(cc), context.Background()}
}

func (serviceClient *portDomainServiceClientImpl) UpsertPorts(ports []*models.PortItem) (int, int, error) {
	nInserted := 0
	nUpdated := 0
	stream, err := serviceClient.rpcClient.UpsertPorts(serviceClient.ctx)
	if err != nil {
		return nInserted, nUpdated, err
	}
	portsItems := models.MapToGrpcPortItem(ports)

	err = stream.Send(&gRPC.UpsertPortsRequest{
		Ports: portsItems,
	})
	if err != nil {
		return nInserted, nUpdated, err
	}
	resp, err := stream.CloseAndRecv()
	if err != nil {
		return nInserted, nUpdated, err
	}
	nInserted = int(resp.TotalInserted)
	nUpdated = int(resp.TotalUpdated)

	return nInserted, nUpdated, nil
}

func (serviceClient *portDomainServiceClientImpl) GetPortsList(limit, skip int32) ([]*models.PortItem, map[string]int, error) {
	stream, err := serviceClient.rpcClient.GetPortsList(serviceClient.ctx, &gRPC.GetPortsRequest{Skip: skip, Limit: limit})
	if err != nil {
		return nil, nil, err
	}

	resp, err := stream.Recv()
	if err != nil {
		return nil, nil, err
	}
	items := models.MapFromGrpcPortItem(resp.Ports)
	paging := map[string]int{
		"total": int(resp.Paging.Total),
		"skip":  int(resp.Paging.Skip),
		"limit": int(resp.Paging.Limit),
	}
	return items, paging, nil
}
