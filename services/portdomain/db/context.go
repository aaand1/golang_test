package db

import (
	"bitbucket.org/aaand1/golang_test/pkg/config"
	"bitbucket.org/aaand1/golang_test/pkg/log"
	"bitbucket.org/aaand1/golang_test/pkg/models"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	stdlog "log"
	"sort"
	"strings"
	"sync"
	"time"
)

type (
	PortItemDoc struct {
		Locode      string     `bson:"_id"`
		Name        string     `bson:"name"`
		City        string     `bson:"city"`
		Country     string     `bson:"country"`
		Alias       []string   `bson:"alias"`
		Regions     []string   `bson:"regions"`
		Coordinates [2]float64 `bson:"coordinates"`
		Province    string     `bson:"province"`
		Timezone    string     `bson:"timezone"`
		Unlocs      []string   `bson:"unlocs"`
		Code        string     `bson:"code"`

		CreatedAt time.Time `bson:"createdAt"`
		UpdatedAt time.Time `bson:"updatedAt"`
	}

	PortItem struct {
		*models.PortItem
		CreatedAt time.Time
	}
)

func (pi *PortItemDoc) ToPortItem() *models.PortItem {
	return &models.PortItem{
		Locode:      pi.Locode,
		Name:        pi.Name,
		City:        pi.City,
		Country:     pi.Country,
		Alias:       pi.Alias,
		Regions:     pi.Regions,
		Coordinates: pi.Coordinates,
		Province:    pi.Province,
		Timezone:    pi.Timezone,
		Unlocs:      pi.Unlocs,
		Code:        pi.Code,
	}
}

func portItemtoBson(model *models.PortItem) *bson.M {
	return &bson.M{
		"name":        model.Name,
		"city":        model.City,
		"country":     model.Country,
		"alias":       model.Alias,
		"regions":     model.Regions,
		"coordinates": model.Coordinates,
		"province":    model.Province,
		"timezone":    model.Timezone,
		"unlocs":      model.Unlocs,
		"code":        model.Code,

		"createdAt": time.Now(),
	}
}

type IDbContext interface {
	InitContext() error
	FindPorts(string, int, int) ([]*models.PortItem, int, error)
	UpsertPorts(model []*models.PortItem) (int, int, error)
}

//Deprecated: Use mongoDbContext instead
type InMemoryDbContext struct {
	ports map[string]*PortItem //TODO: Synchronize with sync.RWMutex
}

var dbContextSingleton IDbContext

func newContext() (IDbContext, error) {
	return &InMemoryDbContext{map[string]*PortItem{}}, nil
}

func Get(appConfig *config.PortDomainConfig) IDbContext {
	if dbContextSingleton != nil {
		return dbContextSingleton
	}

	client, err := mongo.NewClient(options.Client().ApplyURI(appConfig.ConnectionString))
	dbContextSingleton := NewMongoDbContext(client, context.Background())
	if err != nil {
		stdlog.Printf("Failed to initialize db context. Error=%v", err)
		return nil
	}

	return dbContextSingleton
}

func Set(ctx IDbContext) {
	dbContextSingleton = ctx
}

func (ctx *InMemoryDbContext) InitContext() error {
	return nil
}

func (ctx *InMemoryDbContext) FindPorts(countryQuery string, skip, limit int) ([]*models.PortItem, int, error) {
	items := []*models.PortItem{}

	portsSlice := []*PortItem{}
	for _, it := range ctx.ports {
		if countryQuery != "" && !strings.Contains(it.Country, countryQuery) {
			continue
		}
		portsSlice = append(portsSlice, it)
	}

	//Map stable sorting fix
	sort.SliceStable(portsSlice, func(i, j int) bool {
		return portsSlice[i].CreatedAt.Unix() < portsSlice[j].CreatedAt.Unix()
	})
	from, to := skip, skip+limit
	totalPorts := len(portsSlice)
	if from > totalPorts {
		from = totalPorts
	}
	if to > totalPorts {
		to = totalPorts
	}
	portsSlice = portsSlice[from:to]
	for _, it := range portsSlice {
		items = append(items, it.PortItem)
	}

	return items, len(ctx.ports), nil
}

func (ctx *InMemoryDbContext) UpsertPorts(ports []*models.PortItem) (int, int, error) {
	nInserted := 0
	nUpdated := 0
	portsDb := ctx.ports
	for _, it := range ports {
		if len(ctx.ports) == 0 {
			log.Logger.Println("___First " + string(it.Locode))
		}
		if existingItem, ok := (portsDb)[it.Locode]; !ok {
			nInserted++
			(portsDb)[it.Locode] = &PortItem{it, time.Now()}
		} else {
			nUpdated++
			existingItem.PortItem = it
		}
	}

	ctx.ports = portsDb
	return nInserted, nUpdated, nil
}

type mongoDbContext struct {
	Client           *mongo.Client
	clientContext    context.Context
	collectionsCache map[string]*mongo.Collection

	muCollsCache sync.RWMutex
}

func NewMongoDbContext(client *mongo.Client, clientCtx context.Context) IDbContext {
	return &mongoDbContext{
		Client:           client,
		clientContext:    clientCtx,
		collectionsCache: map[string]*mongo.Collection{},

		muCollsCache: sync.RWMutex{},
	}
}

//InitContext creates new context.Context for internal mongo.Client and opens connection to database for client
func (ctx *mongoDbContext) InitContext() error {
	if ctx.clientContext == nil {
		ctx.clientContext, _ = context.WithTimeout(context.Background(), 10*time.Second)
	}

	if err := ctx.Client.Connect(ctx.clientContext); err != nil {
		return err
	}
	if err := ctx.Client.Ping(ctx.clientContext, readpref.PrimaryPreferred()); err != nil {
		return err
	}

	return nil
}

func (ctx *mongoDbContext) getCollection(name string) *mongo.Collection {
	ctx.muCollsCache.RLock()
	col, ok := ctx.collectionsCache[name]
	ctx.muCollsCache.RUnlock()

	if !ok {
		col = ctx.Client.Database("ports").Collection(name)
		ctx.muCollsCache.Lock()
		ctx.collectionsCache[name] = col
		ctx.muCollsCache.Unlock()
	}

	return col
}

func (ctx *mongoDbContext) CountAll(countContext context.Context, filter bson.M) (int64, error) {
	res, err := ctx.getCollection("ports").CountDocuments(countContext, filter)
	return res, err
}

func (ctx *mongoDbContext) FindPorts(countryQuery string, skip, limit int) ([]*models.PortItem, int, error) {
	collection := ctx.getCollection("ports")
	var filter bson.M
	if countryQuery != "" {
		filter["country"] = bson.M{
			"$regexp": fmt.Sprintf("/%v/i", countryQuery), //TODO: Sanitize me
		}
	}
	mongoCtx := context.Background()

	var _limit, _skip int64 = (int64)(limit), (int64)(skip)
	var total int64
	var countErr error
	var countWg sync.WaitGroup
	cancelCtx, cancelFunc := context.WithCancel(context.Background())

	countWg.Add(1)
	go func() {
		total, countErr = ctx.CountAll(cancelCtx, filter)
		countWg.Done()
	}()

	log.Logger.WithFields(logrus.Fields{
		"component": "db/mongoDbContext.FindPorts",
		"skip":      skip,
		"limit":     limit,
		"filter":    filter,
	}).Traceln("db.ports.find() called")
	cur, err := collection.Find(mongoCtx, filter,
		&options.FindOptions{
			Limit: &_limit,
			Skip:  &_skip,
			Sort: bson.M{
				"_id":       1,
				"createdAt": 1,
			},
		})
	if err != nil {
		cancelFunc()
		return nil, 0, err
	}
	countWg.Wait()
	if countErr != nil {
		return nil, 0, countErr
	}
	defer cur.Close(mongoCtx)

	var docs []PortItemDoc
	if err := cur.All(mongoCtx, &docs); err != nil {
		return nil, 0, err
	}
	var ports = make([]*models.PortItem, len(docs))
	for i, e := range docs {
		ports[i] = e.ToPortItem()
	}

	return ports, int(total), nil
}

func (ctx *mongoDbContext) UpsertPorts(ports []*models.PortItem) (int, int, error) {
	collection := ctx.getCollection("ports")
	writes := []mongo.WriteModel{}

	upsert := true
	ordered := true
	for _, it := range ports {
		log.Logger.WithFields(logrus.Fields{
			"it": it,
		}).Tracef("Update for: locode=%+v, coords=%+v\n", it.Locode, it.Coordinates)
		updateModel := mongo.NewUpdateOneModel()
		updateModel.Filter = bson.M{
			"_id": it.Locode,
		}
		updateModel.Update = bson.M{
			"$setOnInsert": portItemtoBson(it),
			"$set": bson.M{
				"updatedAt": time.Now(),
			},
		}
		updateModel.Upsert = &upsert
		writes = append(writes, updateModel)
	}

	res, err := collection.BulkWrite(context.Background(), writes, &options.BulkWriteOptions{
		Ordered: &ordered,
	})
	if err != nil {
		return 0, 0, err
	}

	return int(res.UpsertedCount), int(res.ModifiedCount), nil
}
