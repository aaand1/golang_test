package repositories

import (
	"bitbucket.org/aaand1/golang_test/pkg/models"
	"bitbucket.org/aaand1/golang_test/services/portdomain/db"
)

type IPortsRepository interface {
	UpsertPorts([]*models.PortItem) (int, int, error)
	GetPorts(int, int, string) ([]*models.PortItem, int, error)
}

type portRepository struct {
	dbContext db.IDbContext
}

func NewPortsRepository(dbContext db.IDbContext) IPortsRepository {
	return &portRepository{dbContext}
}

func (repo *portRepository) UpsertPorts(models []*models.PortItem) (int, int, error) {
	nInserted, nUpdated, err := repo.dbContext.UpsertPorts(models)
	return nInserted, nUpdated, err
}

func (repo *portRepository) GetPorts(skip, limit int, countryFilter string) ([]*models.PortItem, int, error) {
	ports, total, err := repo.dbContext.FindPorts(countryFilter, skip, limit)
	return ports, total, err
}
