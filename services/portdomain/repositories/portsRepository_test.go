// +build unit

package repositories

import (
	"bitbucket.org/aaand1/golang_test/mocks"
	"bitbucket.org/aaand1/golang_test/pkg/models"
	"bitbucket.org/aaand1/golang_test/services/portdomain/db"
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	_ "github.com/vektra/mockery"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"testing"
)

type PortsRepositorySuite struct {
	suite.Suite

	ports []*models.PortItem
}

func (s *PortsRepositorySuite) getPorts(sourcePath string) (ports []*models.PortItem) {
	dataPath, _ := filepath.Abs(sourcePath)
	portsData, err := ioutil.ReadFile(dataPath)
	if err != nil {
		log.Fatal(err)
	}

	var portsIndex map[string]*models.PortItem //Temp fix
	if err = json.Unmarshal(portsData, &portsIndex); err != nil {
		log.Fatal(err)
	}
	for locode, it := range portsIndex {
		it.Locode = locode
		ports = append(ports, it)
	}

	return ports
}

func (s *PortsRepositorySuite) SetupSuite() {
}

func (s *PortsRepositorySuite) TearDownSuite() {
}

func (s *PortsRepositorySuite) Test_UpsertPorts() {
	type args struct {
		ports []*models.PortItem
	}

	type expectations struct {
		expectedError error
	}
	cwd, _ := os.Getwd()
	ports := s.getPorts(path.Join(cwd, "../../../test/testdata/ports.json"))

	getPorts := func(from, to int) []*models.PortItem {
		return ports[from:to]
	}

	tests := []struct {
		name         string
		args         *args
		seed         []*models.PortItem
		expectations expectations
	}{
		{
			"It should upsert new ports and proxy dbContext counters",
			&args{
				getPorts(0, 10),
			}, []*models.PortItem{}, expectations{
			errors.New("Connection error"),
		},
		},
	}

	for _, it := range tests {
		s.T().Run(it.name, func(t *testing.T) {
			//Arrange
			var mockDbContext = &mocks.IDbContext{}

			mockDbContext.On("UpsertPorts", it.args.ports).
				Once().
				Return(len(it.args.ports), 0, it.expectations.expectedError)

			//Act
			repo := &portRepository{db.IDbContext(mockDbContext)}
			nUpserted, nUpdated, err := repo.UpsertPorts(it.args.ports)

			//Assert
			if it.expectations.expectedError != nil {
				require.EqualError(t, err, it.expectations.expectedError.Error())
			} else {
				require.NoError(t, err)
			}

			assert.True(t, mockDbContext.AssertExpectations(t))

			require.Equal(t, nUpserted, len(it.args.ports))
			require.Equal(t, nUpdated, 0)
		})
	}
}

func Test_PortsRepository(t *testing.T) {
	suite.Run(t, &PortsRepositorySuite{})
}
