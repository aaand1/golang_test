# README #

This is [assignment](docs/task.md) implementation for golang_test project

## How to run?

### Using demo-all [script](scripts/demo-all.sh)(with docker)
```bash
chmod +x ./scripts/demo-all.sh && $_
```

### Manually(without Docker)
```bash
go mod download
#./build/package/mocks-gen.sh
./build/package/easyjson-gen.sh
./build/package/proto-gen.sh

# Run portdomain service
sudo apt-get install mongodb-org=4.2.1 && service mongodb start # Optional
./build/package/portdomain/test-unit.sh 
env PORT_DOMAIN_SERVICEPORT=8081 PORT_DOMAIN_CONNECTIONSTRING="mongodb://localhost:27017/" go run ./cmd/portdomain/main.go | tail portdomain.log &

# Run clientapi service
env CLIENT_API_SERVICEPORT=8080 CLIENT_API_PORTSDOMAINSERVICEADDRESS="http://localhost:8081/" go run ./cmd/clientapi/main.go | tail clientapi.log &
env CLIENT_API_SERVICE_BASE_URL=http://localhost:8080/ go test -tags=integration ./services/clientapi/...
```

## Requirements
 - golang@1.13.8
 - protoc@3.11.4 with go modules: grpc[+proto-gen-go]/@1.28.0[@latest]
 - mongodb@4.2.3
 - [optional]docker@18.06.0-ce
 - [optional]docker-compose@1.25.4

## Build & Develop
Go module contain 2 services&shared/generated code.
 
```bash
go mod download #install deps for both services
./build/package/easyjson-gen.sh [service-path] #gen REST api json schemas
./build/package/proto-gen.sh #gen GRPC service client/server models encoders/decoders


./build/package/mocks-gen.sh  #generate unit test mocks and run unit tests
go test -race -tags=unit ./services/portdomain/...

# Run services
env CLIENT_API_PORTSDOMAINSERVICEADDRESS=http://localhost:8081/ CLIENT_API_SERVICEPORT=8080 go run ./cmd/clientapi/main.go | tail clientapi.log &
env PORT_DOMAIN_SERVICEPORT=8081 go run ./cmd/portdomain/main.go | tail portdomain.log &

# Run integration tests for clientapi service REST API
env CLIENT_API_SERVICE_BASE_URL=http://localhost:8080/ go test -tags=integration ./services/clientapi/...
```

For docker specific build see: 
 - clientapi multistage [Dockerfile](build/package/clientapi/Dockerfile), RAW integration tests 
 [Docker-compose.test-integration.yaml](build/package/clientapi/Docker-compose.test-integration.yaml)
 - portdomain multistage [Dockerfile](build/package/portdomain/Dockerfile) 
 
 
## TODO: 
 - [out-of-scope]Fix integration tests(ordering bug, counters(1632!=1633 in port_suite<-requires cleanup),add db cleanup for post,get suite)
 - [optional]fix req data validation for REST api & GRPC data 
 - [optional]rewrite POST /api/ports to multipart/form-data with "file=ports.json" request
