module bitbucket.org/aaand1/golang_test

go 1.13

require (
	github.com/gin-gonic/gin v1.5.0
	github.com/golang/protobuf v1.3.4
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mailru/easyjson v0.7.1
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
	github.com/vektra/mockery v0.0.0-20181123154057-e78b021dcbb5
	go.mongodb.org/mongo-driver v1.3.1
	google.golang.org/grpc v1.28.0
)
