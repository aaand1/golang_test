package main

import (
	"bitbucket.org/aaand1/golang_test/internal/gRPC"
	"bitbucket.org/aaand1/golang_test/pkg/config"
	"bitbucket.org/aaand1/golang_test/pkg/impl"
	"bitbucket.org/aaand1/golang_test/pkg/log"
	"bitbucket.org/aaand1/golang_test/services/portdomain/db"
	"bitbucket.org/aaand1/golang_test/services/portdomain/repositories"
	"fmt"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"google.golang.org/grpc"
	stdlog "log"
	"net"
	"os"
	"os/signal"
)

func main() {
	var appConfig = new(config.PortDomainConfig)
	_, err := config.Init("PORT_DOMAIN", appConfig)
	stdlog.Print("Initialized config: ", appConfig)
	if err != nil {
		stdlog.Fatal(err)
	}
	output := os.Stdout
	log.Init(appConfig.LogLevel, output, appConfig.LogPath, nil)
	log.Logger.Debugf("Initialized logger: level=%s, output=%s; logger=%v\n", appConfig.LogLevel, output, log.Logger)

	dbContext := db.Get(appConfig)
	err = dbContext.InitContext()
	if err != nil {
		log.Logger.Fatal(err)
	}

	log.Logger.Printf("Starting PortDomain grpc server at: %s:%d ...\n", appConfig.ServiceHost, appConfig.ServicePort)

	netListener, err := getNetListener(appConfig.ServiceHost, appConfig.ServicePort)
	if err != nil {
		log.Logger.Fatalf("failed to listen: %v", err)
	}
	gRPCServer := grpc.NewServer(grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
		grpc_recovery.StreamServerInterceptor(),
	)), grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
		grpc_recovery.UnaryServerInterceptor(),
	)))
	portDomainServiceImpl := impl.NewPortDomainServiceGrpcImpl(repositories.NewPortsRepository(dbContext))
	gRPC.RegisterPortDomainServiceServer(gRPCServer, portDomainServiceImpl)

	shutdownChannel := make(chan os.Signal, 1)
	go listenShutdownSignal(shutdownChannel, func(shutdownSignal os.Signal) {
		log.Logger.Println("Shutting down the server...")
		gRPCServer.GracefulStop()
	})
	// start the server
	if err := gRPCServer.Serve(netListener); err != nil {
		log.Logger.Fatalf("failed to serve: %s", err)
	}
}

func getNetListener(host string, port uint) (net.Listener, error) {
	lis, err := net.Listen("tcp", fmt.Sprintf("%v:%d", host, port))
	if err != nil {
		return nil, err
	}
	return lis, nil
}

func listenShutdownSignal(inputChannel chan os.Signal, onShutdown func(os.Signal)) {
	signal.Notify(inputChannel, os.Kill, os.Interrupt)

	inputSignal := <-inputChannel
	if inputSignal == os.Kill || inputSignal == os.Interrupt {
		onShutdown(inputSignal)
	}
}
