package main

import (
	"bitbucket.org/aaand1/golang_test/pkg/config"
	"bitbucket.org/aaand1/golang_test/pkg/log"
	"bitbucket.org/aaand1/golang_test/services/clientapi/controller"
	"context"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	stdlog "log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"
)

func main() {
	var appConfig = new(config.ClientAPIConfig)
	_, err := config.Init("CLIENT_API", appConfig)
	stdlog.Print("Initialized config: ", appConfig)
	if err != nil {
		stdlog.Fatal(err)
	}
	output := os.Stdout
	log.Init(appConfig.LogLevel, output, appConfig.LogPath, nil)
	log.Logger.Debugf("Initialized logger: level=%s, output=%s; logger=%v\n", appConfig.LogLevel, output, log.Logger)

	gin.SetMode(appConfig.GinMode)
	gin.DefaultWriter = log.Logger.Writer()

	ctx, cancelFunc := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancelFunc()
	conn, e := grpc.DialContext(ctx, appConfig.PortDomainServiceAddress.Host, grpc.WithInsecure())
	if e != nil {
		log.Logger.Fatal(e)
	}
	defer conn.Close()

	apiRouter, appRouter := gin.Default(), gin.Default()
	controller.Startup(conn, appRouter, apiRouter)
	server := http.Server{
		Addr:    appConfig.ServiceHost + ":" + strconv.FormatUint(uint64(appConfig.ServicePort), 10),
		Handler: apiRouter,
	}

	log.Logger.Printf("Starting Client-api http server at: %s:%d ...\n", appConfig.ServiceHost, appConfig.ServicePort)

	shutdownChannel := make(chan os.Signal, 1)
	go listenShutdownSignal(shutdownChannel, func(shutdownSignal os.Signal) {
		log.Logger.Println("Shutting down the server...")

		ctx, cancelFunc := context.WithTimeout(context.Background(), time.Second*5)
		defer cancelFunc()
		if err := server.Shutdown(ctx); err != nil {
			log.Logger.WithError(err).Error("server.Shutdown error: ")
		}
	})
	log.Logger.Fatal(server.ListenAndServe())
}

func listenShutdownSignal(inputChannel chan os.Signal, onShutdown func(os.Signal)) {
	signal.Notify(inputChannel, os.Kill, os.Interrupt)

	inputSignal := <-inputChannel
	if inputSignal == os.Kill || inputSignal == os.Interrupt {
		onShutdown(inputSignal)
	}
}
