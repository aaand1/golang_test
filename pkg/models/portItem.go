package models

import (
	"bitbucket.org/aaand1/golang_test/internal/gRPC"
	"bitbucket.org/aaand1/golang_test/pkg/log"
	"github.com/sirupsen/logrus"
	"strings"
)

type PortItem struct {
	Locode      string     `json:"locode"`
	Name        string     `json:"name"`
	City        string     `json:"city"`
	Country     string     `json:"country"`
	Alias       []string   `json:"alias"`
	Regions     []string   `json:"regions"`
	Coordinates [2]float64 `json:"coordinates"`
	Province    string     `json:"province"`
	Timezone    string     `json:"timezone"`
	Unlocs      []string   `json:"unlocs"`
	Code        string     `json:"code"`
}

func MapFilterFromGrpcPortItem(grpcModels []*gRPC.PortItem, logger log.IAppLogger) (models []*PortItem) {
	for _, it := range grpcModels {
		if strings.Trim(it.Timezone, " ") == "" {
			log.Logger.WithFields(logrus.Fields{
				"caller":   "models.MapFilterFromGrpcPortItem",
				"portItem": it,
			}).Warnf("Empty timezone. locode=%v, timezone=%v", it.Locode, it.Timezone)
		}
		if len(it.Coordinates) < 2 {
			logger.WithFields(logrus.Fields{
				"caller":   "models.MapFilterFromGrpcPortItem",
				"portItem": it,
			}).Warnf("Invalid coordinates. Skipping item: locode=%v, coordinates=%v\n", it.Locode, it.Coordinates)
			continue
		}
		coordinates := [2]float64{it.Coordinates[0], it.Coordinates[1]}

		models = append(models, &PortItem{
			Locode:      it.Locode,
			Name:        it.Name,
			City:        it.City,
			Country:     it.Country,
			Alias:       normalizeSlice(it.Alias),
			Regions:     normalizeSlice(it.Regions),
			Coordinates: coordinates,
			Province:    it.Province,
			Timezone:    it.Timezone,
			Unlocs:      normalizeSlice(it.Unlocs),
			Code:        it.Code,
		})
	}
	return
}

func MapFromGrpcPortItem(grpcModels []*gRPC.PortItem) []*PortItem {
	models := make([]*PortItem, len(grpcModels))
	for i, it := range grpcModels {
		coordinates := [2]float64{it.Coordinates[0], it.Coordinates[1]} //TODO: repeated can have length in *.proto?
		models[i] = &PortItem{
			Locode:      it.Locode,
			Name:        it.Name,
			City:        it.City,
			Country:     it.Country,
			Alias:       normalizeSlice(it.Alias),
			Regions:     normalizeSlice(it.Regions),
			Coordinates: coordinates,
			Province:    it.Province,
			Timezone:    it.Timezone,
			Unlocs:      normalizeSlice(it.Unlocs),
			Code:        it.Code,
		}
	}
	return models
}

func MapToGrpcPortItem(models []*PortItem) []*gRPC.PortItem {
	grpcModels := make([]*gRPC.PortItem, len(models))
	for i, it := range models {
		coordinates := []float64{it.Coordinates[0], it.Coordinates[1]}
		grpcModels[i] = &gRPC.PortItem{
			Locode:      it.Locode,
			Name:        it.Name,
			City:        it.City,
			Country:     it.Country,
			Alias:       it.Alias,
			Regions:     it.Regions,
			Coordinates: coordinates,
			Province:    it.Province,
			Timezone:    it.Timezone,
			Unlocs:      it.Unlocs,
			Code:        it.Code,
		}
	}
	return grpcModels
}

//normalizeSlice prevent insertion of nil slice(and it's propagation to models.PortItem field)
//due to pb/proto empty slice unmarshalling
func normalizeSlice(slice []string) []string {
	if slice == nil {
		slice = []string{}
	}
	return slice
}
