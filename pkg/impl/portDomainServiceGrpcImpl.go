package impl

import (
	"bitbucket.org/aaand1/golang_test/internal/gRPC"
	"bitbucket.org/aaand1/golang_test/pkg/log"
	"bitbucket.org/aaand1/golang_test/pkg/models"
	"bitbucket.org/aaand1/golang_test/services/portdomain/repositories"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type PortDomainServiceGrpcImpl struct {
	srv  gRPC.UnimplementedPortDomainServiceServer
	repo repositories.IPortsRepository
}

func NewPortDomainServiceGrpcImpl(portsRepo repositories.IPortsRepository) gRPC.PortDomainServiceServer {
	return &PortDomainServiceGrpcImpl{repo: portsRepo}
}

func (service *PortDomainServiceGrpcImpl) UpsertPorts(stream gRPC.PortDomainService_UpsertPortsServer) error {
	reqMsg, err := stream.Recv()
	if err != nil {
		return err
	}

	nReceived := len(reqMsg.Ports)
	ports := models.MapFilterFromGrpcPortItem(reqMsg.Ports, log.Logger)

	nInserted, nUpdated, err := service.repo.UpsertPorts(ports)
	if err != nil {
		log.Logger.WithFields(logrus.Fields{
			"error": err,
		}).Errorln("Insert error occurred")
		return status.New(codes.Internal, "INSERT_ERROR").Err()
	}
	log.Logger.Debugf("Insert %v, update %v out of %v ports", nInserted, nUpdated, nReceived)

	if (nInserted + nUpdated) != nReceived {
		log.Logger.Warnf("%v items wasn't inserted during request processing.", nReceived-(nInserted+nUpdated))
	}

	return stream.SendAndClose(&gRPC.UpsertPortsResponse{
		TotalInserted: int32(nInserted),
		TotalUpdated:  int32(nUpdated),
	})
}

func (service *PortDomainServiceGrpcImpl) GetPortsList(req *gRPC.GetPortsRequest, stream gRPC.PortDomainService_GetPortsListServer) error {
	limit, skip := req.Limit, req.Skip
	if limit <= 0 {
		return status.New(codes.InvalidArgument, "INVALID_LIMIT_ERROR").Err()
	}
	if skip < 0 {
		skip = 0
	}
	portsModels, totalPorts, err := service.repo.GetPorts(int(skip), int(limit), "")
	if err != nil {
		return status.New(codes.Internal, "GET_ERROR").Err()
	}

	ports := models.MapToGrpcPortItem(portsModels)

	if err = stream.Send(&gRPC.GetPortsResponse{
		Ports: ports,
		Paging: &gRPC.PagingResponse{
			Skip: skip,
			Limit:  limit,
			Total:  int32(totalPorts),
		},
	}); err != nil {
		return err
	}

	return nil
}
