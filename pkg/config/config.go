package config

import (
	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
	"net/url"
	"sync"
)

var (
	configsSyncMap = new(sync.Map)
)

type ConfigUrlDecoder url.URL

func (urlDecoder *ConfigUrlDecoder) Decode(rawUrl string) error {
	value, err := url.ParseRequestURI(rawUrl)
	if err != nil {
		return err
	}

	*urlDecoder = ConfigUrlDecoder(*value)

	return nil
}

func (urlDecoder *ConfigUrlDecoder) String() string {
	var rawUrl = url.URL(*urlDecoder)
	return rawUrl.String()
}

type AppConfig struct {
	ServiceHost string       `default:"localhost"`
	ServicePort uint         `default:"80"`
	LogLevel    logrus.Level `default:"info"`
	LogPath     string       `default:""`
}

type ClientAPIConfig struct {
	AppConfig
	GinMode                  string           `default:"debug"'` //one of "debug", "release", "test"
	PortDomainServiceAddress ConfigUrlDecoder `required:"true"`  //eg: http://localhost:8081/
}

type PortDomainConfig struct {
	AppConfig
	ConnectionString string `default:"mongodb://localhost:27017"`
}

func Init(prefix string, config interface{}) (interface{}, error) {
	if cfg, ok := configsSyncMap.Load(prefix); ok {
		return cfg, nil
	}
	err := envconfig.Process(prefix, config)
	if err != nil {
		return nil, err
	}

	Set(prefix, config)
	return config, nil
}

func Get(prefix string) interface{} {
	config, _ := configsSyncMap.Load(prefix)
	return config
}

func Set(prefix string, cfg interface{}) {
	configsSyncMap.Store(prefix, cfg)
}
