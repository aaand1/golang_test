package log

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"io"
	"log"
	"os"
	"path"
	"strings"
)

var Logger IAppLogger = NewLogger(logrus.StandardLogger().Level, logrus.StandardLogger().Formatter, os.Stdout)

func Init(level logrus.Level, output io.Writer, outputLogPath string, formatter logrus.Formatter) {
	if strings.TrimSpace(outputLogPath) != "" {
		var outputFilePath string = outputLogPath
		if fi, err := os.Stat(outputLogPath); err == nil && fi.Mode().IsRegular() {
			outputFilePath = outputLogPath
		} else if err == nil && fi.Mode().IsDir() {
			outputFilePath = fmt.Sprintf("%s/stdout.log", outputLogPath)
		}

		if !path.IsAbs(outputFilePath) {
			cwd, _ := os.Getwd()
			outputFilePath = path.Join(cwd, outputLogPath)
		}
		if fileOutput, err := os.OpenFile(outputFilePath, os.O_WRONLY|os.O_CREATE, 0755); err != nil {
			log.Fatalln("logger.Init error: ", err, outputLogPath)
		} else {
			output = fileOutput
			//defer fileOutput.Close()
		}
	}
	Logger.SetLevel(level)
	if formatter != nil {
		Logger.SetFormatter(formatter)
	}
	Logger.SetOutput(output)
}

type IAppLogger interface {
	//logrus.StdLogger //FieldLogger conflict, there no std log compatibility
	SetLevel(logrus.Level)
	SetOutput(io.Writer)
	SetFormatter(logrus.Formatter)
	Writer() io.Writer
	logrus.FieldLogger

	DbLogger() IAppLogger
}

func NewLogger(level logrus.Level, formatter logrus.Formatter, output io.Writer) IAppLogger {
	std := logrus.New()
	std.SetLevel(level)
	std.SetFormatter(formatter)
	std.SetOutput(output)

	return &AppLogger{*std}
}

type AppLogger struct {
	logrus.Logger
}

func (l *AppLogger) Writer() io.Writer {
	return l.Logger.Writer()
}

func (l *AppLogger) Print(values ...interface{}) {
	l.Logger.Debug(values...)
}

func (l *AppLogger) DbLogger() IAppLogger {
	return l
}
