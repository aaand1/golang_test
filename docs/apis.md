## ClientAPI service REST API

| Method | Path                             | Description                 | Body                                                                       | Sample response (CODE; CONTENT-TYPE BODY)                                                                                                                                            |
|--------|----------------------------------|-----------------------------|----------------------------------------------------------------------------| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| POST   | /api/ports                       | Upserts batch of ports      | application/json: {"AEAJM": {"name": "Ajman", "city": "Ajman", ...}, ... } | 200/201 application/json { "stats": { "totalInserted": 1622, "totalUpdated": 0 } }  400/500 application/json INVALID_DATA|INTERNAL_SERVER_ERROR                                      |
| GET    | /api/ports[?limit=30[&skip=0]]   | Get all ports               |                                                                            | 200 { "paging": { "total": 1622, "skip": 0, "limit": 30 }, "items": [{"locode": "AEAJM", "name": "Ajman", ...] } 400/500 application/json INVALID_QUERY_PARAMS|INTERNAL_SERVER_ERROR |


# PortDomain service GRPC API

See [.proto](../api/port_domain_service.proto) [files](../api/port_item.proto) for service schemas/description.  