#!/usr/bin/env bash

clientapi_service_host="localhost"
if [ -n "$CLIENT_API_SERVICEHOST" ]; then
    clientapi_service_host="$CLIENT_API_SERVICEHOST"
fi

clientapi_service_port="8080"
if [ -n "$CLIENT_API_SERVICEPORT" ]; then
    clientapi_service_port="$CLIENT_API_SERVICEPORT"
fi

portdomain_service_address="http://localhost:8081/"
if [ -n "$CLIENT_API_PORTDOMAINSERVICEADDRESS" ]; then
    portdomain_service_address="$CLIENT_API_PORTDOMAINSERVICEADDRESS"
fi

clientapi_service_base_url="http://$clientapi_service_host:$clientapi_service_port/"
if [ -n "$CLIENT_API_SERVICE_BASE_URL" ]; then
    clientapi_service_base_url="$CLIENT_API_SERVICE_BASE_URL"
fi

env CLIENT_API_PORTDOMAINSERVICEADDRESS="$portdomain_service_address" \
    CLIENT_API_SERVICEHOST=$clientapi_service_host CLIENT_API_SERVICEPORT=$clientapi_service_port \
    CLIENT_API_LOGLEVEL=trace ./main.run &
sleep 6s

echo "----Starting integration tests: CLIENT_API_SERVICE_BASE_URL=$clientapi_service_base_url..."
env CLIENT_API_SERVICE_BASE_URL="$clientapi_service_base_url" go test -race -tags=integration ./services/clientapi/...

