#!/usr/bin/env sh

GEN_PATHS="./services/clientapi/viewmodels"

if [ -n "$1" ]; then
    GEN_PATHS="$1"
fi

if ! [ -x "$(command -v easyjson)" ]; then
    go get github.com/mailru/easyjson/...
fi

for P in $GEN_PATHS ; do
echo "gen easyjson for $P"
easyjson -pkg $P
done
