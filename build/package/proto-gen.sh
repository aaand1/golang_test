#!/usr/bin/env sh

install_protoc() {
    # https://gist.github.com/vaijab/80e30d1d695b7f725a4fba43ef5849d8
    export PROTOC_VERSION=3.11.4
    wget -q https://github.com/google/protobuf/releases/download/v${PROTOC_VERSION}/protoc-${PROTOC_VERSION}-linux-x86_64.zip -O /tmp/protoc-${PROTOC_VERSION}.zip && unzip -o -d /usr /tmp/protoc-${PROTOC_VERSION}.zip
}

if ! [ -x "$(command -v protoc)" ]; then
    install_protoc
    go get google.golang.org/grpc
    go get github.com/golang/protobuf/protoc-gen-go
fi

protoc -I ./api --go_out=paths=source_relative,plugins=grpc:./internal/gRPC ./api/*.proto