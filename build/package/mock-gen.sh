#!/usr/bin/env sh

MOCK_PATH="./services/portdomain ./services/clientapi"

if [ -n "$1" ]; then
    MOCK_PATH="$1"
fi

if ! [ -x "$(command -v mockery)" ]; then
    go get github.com/vektra/mockery/...
fi

for P in $MOCK_PATH ; do
echo "gen mocks for $P"
mockery -dir $P -all -recursive
done